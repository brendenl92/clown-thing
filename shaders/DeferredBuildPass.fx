uniform extern float4x4 World;
uniform extern float4x4 VP;
uniform extern float4x4 WorldInvTrans;

uniform extern float4x4 lightWVP;

uniform extern texture Tex;

struct BuildVSIn
{
	float4  Pos             : POSITION0;
	float2  tex0            : TEXCOORD0;
	float3  Norm            : NORMAL0;
};

struct BuildVSOut
{
	float4	Pos			    : POSITION0;
	float2  tex0    	    : TEXCOORD0;
	float3	WPos			: TEXCOORD1;
	float3	WNorm			: TEXCOORD2;
	float4  LPos			: TEXCOORD4;
	float   Depth : TEXCOORD3;
};

struct BuildPSOut
{
	float4	Diffuse			: COLOR0;
	float4	Norm			: COLOR1;
	float4	Position		: COLOR2;
	float4	Shadow			: COLOR3;
};

sampler TexS = sampler_state
{
	Texture = <Tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 16;
	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = Wrap;
};

BuildVSOut BuildVS(BuildVSIn input)
{
	BuildVSOut output = (BuildVSOut)0;

	output.WPos = mul(input.Pos, World).xyz;
	output.Pos = mul(float4(output.WPos, 1), VP);
	output.tex0 = input.tex0;
	output.WNorm = normalize(mul(float4(input.Norm, 0), WorldInvTrans)).xyz;
	output.LPos = mul(input.Pos, lightWVP);
	output.Depth = output.LPos.z;

	return output;
};

BuildPSOut BuildPS(BuildVSOut input) : COLOR0
{
	BuildPSOut output = (BuildPSOut)0;

	float4 SampledDiffuse = tex2D(TexS, input.tex0);

		output.Diffuse.rgb = SampledDiffuse.rgb;
	output.Diffuse.a = .1;

	output.Norm.xyz = input.WNorm * 0.5 + 0.5;
	output.Norm.w = 0.0;

	output.Position = float4(input.WPos.xyz, 0);

	output.Shadow = float4(input.Depth, input.Depth, input.Depth, 1.0f);

	return output;
};

technique main
{
	pass p0
	{
		VertexShader = compile vs_2_0 BuildVS();
		PixelShader = compile ps_2_0 BuildPS();

		CullMode = none;
		FillMode = solid;
		Zenable = true;
		ZWriteEnable = true;
		ZFunc = less;
		StencilEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = true;
		ColorWriteEnable = red | green | blue;
	}
}