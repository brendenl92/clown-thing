uniform extern float4 MatAmbient;
uniform extern float4 MatDiffuse;
uniform extern float4 MatSpecular;
uniform extern float  MatPower;

uniform extern float4 LightDiffuse;
uniform extern float4 LightSpecular;
uniform extern float3 LightPosition;
uniform extern float3 LightDirection;

uniform extern float3 EyePosW;

uniform extern float3 fogColor;
uniform extern float  FogDensity;

uniform extern texture DiffuseMap;
uniform extern texture NormalMap;
uniform extern texture PositionMap;
uniform extern texture DepthMap;

sampler DiffuseSampler = sampler_state
{
	Texture = <DiffuseMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler NormalSampler = sampler_state
{
	Texture = <NormalMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler PositionSampler = sampler_state
{
	Texture = <PositionMap>;
	MagFilter = Point;
	MinFilter = Point;
};

sampler DepthSampler = sampler_state
{
	Texture = <DepthMap>;
	MagFilter = Point;
	MinFilter = Point;
};

struct LightPSInput
{
	float2 tex0		: TEXCOORD0;
};

float4 LightPS(LightPSInput input) : COLOR0
{

	float3 Diffuse = tex2D(DiffuseSampler, input.tex0).rgb;
	float3 Specular = tex2D(DiffuseSampler, input.tex0).a;
	//float lit = tex2D(DiffuseSampler, input.tex0).a;

	float3 WNorm = ((tex2D(NormalSampler, input.tex0) - 0.5) * 2).xyz;

	float3 WPos;
	WPos.xyz = tex2D(PositionSampler, input.tex0).xyz;

	float3 LightDir = normalize(LightPosition - WPos).xyz;

		float3 lightVecW = -LightDirection;

		float3 r = reflect(-lightVecW, WNorm);

		float3 EyeVec = normalize(EyePosW - WPos);

		float t = pow(max(dot(r, EyeVec), 0.0f), MatPower);

	float s = max(dot(lightVecW, WNorm), 0.0f);

	float3 spec = t * (MatSpecular * LightSpecular).rgb;

		float3 diffuse = s * (MatDiffuse * (LightDiffuse * LightDiffuse)).rgb;

		float3 ambient = MatAmbient.rgb * LightDiffuse.rgb;

		float dist = length(WPos - EyePosW);

	float L = exp(-dist * FogDensity);
	L = saturate(1 - L);

	float3 color;

	if (WPos.z > 1000)
	{
		color = Diffuse.rgb;
	}
	else
	{
		color = (diffuse + diffuse) * Diffuse.rgb + spec;
		//color = float3(1, 0, 1);
	}

	return float4(WPos.xyz, 1);

};

technique main
{
	pass p0
	{
		VertexShader = NULL;
		PixelShader = compile ps_2_0 LightPS();

		CullMode = none;
		FillMode = solid;
		Zenable = false;
		StencilEnable = false;
		AlphaBlendEnable = true;
		Srcblend = One;
		Destblend = One;
		AlphaTestEnable = false;
		ColorWriteEnable = red | green | blue;
	}
};
//
//float4x4 g_mWorldView;
//float4x4 g_mProj;
//float4x4 g_mViewToLightProj;
//float4   g_vMaterial;
//texture  g_txScene;
//texture  g_txShadow;
//float3   g_vLightPos;
//float3   g_vLightDir;
//float4   g_vLightDiffuse = float4(1.0f, 1.0f, 1.0f, 1.0f);
//float4   g_vLightAmbient = float4(0.3f, 0.3f, 0.3f, 1.0f);
//float    g_fCosTheta;
//
//sampler2D g_samScene = sampler_state
//{
//	Texture = <g_txScene>;
//	MinFilter = Point;
//	MagFilter = Linear;
//	MipFilter = Linear;
//};
//
//sampler2D g_samShadow = sampler_state
//{
//	Texture = <g_txShadow>;
//	MinFilter = Point;
//	MagFilter = Point;
//	MipFilter = Point;
//	AddressU = Clamp;
//	AddressV = Clamp;
//};
//
//void VertScene(float4 iPos : POSITION, float3 iNormal : NORMAL, float2 iTex : TEXCOORD0, out float4 oPos : POSITION, out float2 Tex : TEXCOORD0, out float4 vPos : TEXCOORD1,
//				out float3 vNormal : TEXCOORD2, out float4 vPosLight : TEXCOORD3)
//{
//	vPos = mul(iPos, g_mWorldView);
//
//	oPos = mul(vPos, g_mProj);
//
//	vNormal = mul(iNormal, (float3x3)g_mWorldView);
//
//	Tex = iTex;
//
//	vPosLight = mul(vPos, g_mViewToLightProj);
//}
//
//float4 PixScene(float2 Tex : TEXCOORD0, float4 vPos : TEXCOORD1, float3 vNormal : TEXCOORD2, float4 vPosLight : TEXCOORD3) : COLOR
//{
//	float4 Diffuse;
//
//	float3 vLight = normalize(float3(vPos - g_vLightPos));
//
//		if (dot(vLight, g_vLightDir) > g_fCosTheta) // Light must face the pixel (within Theta)
//		{
//
//			float2 ShadowTexC = 0.5 * vPosLight.xy / vPosLight.w + float2(0.5, 0.5);
//			ShadowTexC.y = 1.0f - ShadowTexC.y;
//
//			float2 texelpos = SMAP_SIZE * ShadowTexC;
//
//			float2 lerps = frac(texelpos);
//
//			float sourcevals[4];
//			sourcevals[0] = (tex2D(g_samShadow, ShadowTexC) + SHADOW_EPSILON < vPosLight.z / vPosLight.w) ? 0.0f : 1.0f;
//			sourcevals[1] = (tex2D(g_samShadow, ShadowTexC + float2(1.0 / SMAP_SIZE, 0)) + SHADOW_EPSILON < vPosLight.z / vPosLight.w) ? 0.0f : 1.0f;
//			sourcevals[2] = (tex2D(g_samShadow, ShadowTexC + float2(0, 1.0 / SMAP_SIZE)) + SHADOW_EPSILON < vPosLight.z / vPosLight.w) ? 0.0f : 1.0f;
//			sourcevals[3] = (tex2D(g_samShadow, ShadowTexC + float2(1.0 / SMAP_SIZE, 1.0 / SMAP_SIZE)) + SHADOW_EPSILON < vPosLight.z / vPosLight.w) ? 0.0f : 1.0f;
//
//			float LightAmount = lerp(lerp(sourcevals[0], sourcevals[1], lerps.x), lerp(sourcevals[2], sourcevals[3], lerps.x), lerps.y);
//			Diffuse = (saturate(dot(-vLight, normalize(vNormal))) * LightAmount * (1 - g_vLightAmbient) + g_vLightAmbient) * g_vMaterial;
//		}
//		else
//		{
//			Diffuse = g_vLightAmbient * g_vMaterial;
//		}
//
//	return tex2D(g_samScene, Tex) * Diffuse;
//}
//
//technique RenderScene
//{
//	pass p0
//	{
//		VertexShader = compile vs_2_0 VertScene();
//		PixelShader = compile ps_2_0 PixScene();
//	}
//}
