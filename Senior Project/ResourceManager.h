#pragma once
#include "mesh.h"
#include <vector>

enum FXID {BUILDFX, LIGHTFX, SKYFX, FINALFX, SHADOWFX};

class ResourceManager
{
	vector<Mesh*> MeshPool;
	LPD3DXSPRITE m_Sprite;
	LPD3DXLINE m_Line;
	vector<LPDIRECT3DTEXTURE9*> TexturePool;
	vector<D3DXIMAGE_INFO> TextureInfo;
	vector<LPD3DXFONT> FontPool;
	vector<D3DXVECTOR2> FontSize;
	

public:
	ResourceManager();
	~ResourceManager();

	UINT ImportMeshes(string* name, int numEntries);
	void DeleteMesh(int id);
	
	Mesh* getMesh(int id);
	UINT doesMeshExist(string* name);

	void ImportTextures(string* name, int numEntries);
	void DeleteTexture(int id);
	void updateTextureScale(D3DXVECTOR2 scale);

	LPDIRECT3DTEXTURE9* getTexture(int id);	
	
	int getTextureX(int id);
	int getTextureY(int id);

	LPD3DXSPRITE getSprite();
	LPD3DXLINE getLine();

	LPD3DXFONT getFont(int i);

	int createFont(string name, UINT height, UINT width, UINT weight, bool italic);
	D3DXVECTOR2 getFontSize(int i);
	void deleteFont(int i);

	void clearAll();

	void onLostDevice();
	void onResetDevice();
};  