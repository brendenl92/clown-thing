#pragma once
#include "../inc/fmod.hpp"
#include "../inc/fmod.h"
#include "fmod_errors.h"
#include "../inc/fmod_dsp.h"
#include "../inc/fmod_output.h"
#include <vector>
#include "mailbox.h"
#include <string>

#pragma comment(lib, "fmodex_vc.lib")

using namespace std;
class Audio
{
	FMOD::System* system;
	FMOD::Sound* music;
	FMOD::Channel* musicChannel;
	FMOD::Reverb* reverb;
	vector<pair<FMOD::Sound*, float>> effects;
	MailBox* m_MailBox;

public:
	Audio();
	~Audio();
	void startup();
	void update();
	void playSound(string name);
	void addMessage(Mail* mail);
};

