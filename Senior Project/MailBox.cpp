#include "MailBox.h"


MailBox::MailBox()
{

}


MailBox::~MailBox()
{

}

Mail* MailBox::getNext()
{
	Mail* temp;

	if (MailBin.size() > 0)
	{
		temp = MailBin[0];
		MailBin.erase(MailBin.begin());
	}
	else
		temp = nullptr;



	return temp;
}

void MailBox::setMail(Mail* in)
{
	if (in->priority == HIGH)
		MailBin.insert(MailBin.begin(), in);
	else if (in->priority == MEDIUM)
	{
		for (unsigned int i = 0; i < MailBin.size(); i++)
			if (MailBin[i]->priority == MEDIUM)
				MailBin.insert(MailBin.begin() + i, in);
	}
	else
		MailBin.push_back(in);

}