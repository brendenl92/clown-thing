#include "Log.h"
#include <ctime>
#include <time.h>

Log* LOG = 0;

Log::Log()
{

}


Log::~Log()
{

}

void Log::send()
{
	//ofstream OutFile;
	OutFile.open("errors.txt", fstream::out);
	if (!OutFile.good())
	{
		//not opening right
	}

	for (unsigned int i = 0; i < logs.size(); i++)
		OutFile << logs[i] << std::endl;

	OutFile.close();

	logs.clear();
}

void Log::log(string in)
{
	time_t rawtime;
	struct tm timeinfo;

	localtime_s(&timeinfo, &rawtime);

	SYSTEMTIME st;
	GetSystemTime(&st);
	
	string str;
	
	str.push_back((char)st.wHour);
	str.push_back(':');
	str.push_back((char)st.wMinute);
	str.push_back(' ');

	for (unsigned int x = 0; x < in.size(); ++x)
		str.push_back(in[x]);

	logs.push_back(str);
	
	if (logs.size() == 10)
		this->send();
}