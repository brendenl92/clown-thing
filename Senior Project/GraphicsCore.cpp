#include "GraphicsCore.h"
#include "Kernel.h"
#include "ResourceManager.h"
GraphicsCore::GraphicsCore()
{

}

GraphicsCore::~GraphicsCore()
{

}

void GraphicsCore::initialize()
{
	m_WindowPosition[0] = 0;
	m_WindowPosition[1] = 0;
	m_Resolution[0] = GetSystemMetrics(SM_CXSCREEN);
	m_Resolution[1] = GetSystemMetrics(SM_CYSCREEN);

	planeVerts[0] = { 0.0f, 0.0f, .5f, 1, 0 + .5f / m_Resolution[0], 0.0f + .5f / m_Resolution[1] };
	planeVerts[1] = { (float)m_Resolution[0], 0.0f, .5f, 1.0f, 1.0f + .5f / (float)m_Resolution[0], 0.0f + .5f / (float)m_Resolution[1] };
	planeVerts[2] = { (float)m_Resolution[0], (float)m_Resolution[1], .5f, 1.0f, 1.0f + .5f / (float)m_Resolution[0], 1.0f + .25f / (float)m_Resolution[1] };
	planeVerts[3] = { 0.0f, (float)m_Resolution[1], .5f, 1.0f, 0 + .5f / (float)m_Resolution[0], 1.0f + .5f / (float)m_Resolution[1] };//Plane Verts

	m_DevType = D3DDEVTYPE_HAL;
	m_RequestedVP = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	m_MainWndInst = NULL;
	m_d3Object = NULL;
	//ZeroMemory(&m_D3dPP, sizeof(m_D3dPP));

	WNDCLASS wc;
	wc.lpszClassName = "420window";
	wc.hInstance = KRNL->getInstance();
	wc.lpfnWndProc = MainWndProc;
	wc.style = CS_HREDRAW | CS_VREDRAW;  //redraw if window is resized
	wc.cbClsExtra = 0;					 //extra bytes after classstructure
	wc.cbWndExtra = 0;					 //extra bytes after windowinstance
	wc.hIcon = 0;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;

	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, "RegisterClass Failed", "Register Failure", MB_ICONWARNING);
		PostQuitMessage(0);
	}//Windows Class

	RECT rect = { 0, 0, m_Resolution[0], m_Resolution[1] };
	AdjustWindowRect(&rect, WS_POPUP, false);
	m_MainWndInst = CreateWindowA("420window", "GSP 420 Blays et", WS_POPUP, 0, 0, rect.right, rect.bottom, NULL, NULL, NULL, NULL);
	if (!m_MainWndInst)
	{
		DWORD err = GetLastError();
		MessageBox(0, "Create Window Failed", "error", MB_ICONWARNING);
		PostQuitMessage(0);
	}
	SetWindowLongA(m_MainWndInst, GWL_STYLE, WS_POPUP); //sets window to popup windowstyle
	SetWindowPos(m_MainWndInst, HWND_TOP, 0, 0, rect.right, rect.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	ShowWindow(m_MainWndInst, SW_SHOW);
	UpdateWindow(m_MainWndInst);//Window Creation

	m_d3Object = Direct3DCreate9(D3D_SDK_VERSION); //Creates Direct3D Interface
	if (!m_d3Object)
	{
		MessageBox(NULL, "Direct Object Failed!", "Failure!", MB_ICONWARNING);
		PostQuitMessage(0);
	}

	D3DDISPLAYMODE mode;
	m_d3Object->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	m_d3Object->CheckDeviceType(D3DADAPTER_DEFAULT, m_DevType, mode.Format, mode.Format, true); //checks windowed format
	m_d3Object->CheckDeviceType(D3DADAPTER_DEFAULT, m_DevType, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false); //checks fullscreen format

	D3DCAPS9 caps;
	m_d3Object->GetDeviceCaps(D3DADAPTER_DEFAULT, m_DevType, &caps);

	DWORD behaviorFlags = NULL;
	if (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) //tests for hardware vertex processing
	{
		behaviorFlags |= m_RequestedVP;			//if so it assigns hardware processing flag(passed in from winmain)
		if (caps.DevCaps & D3DDEVCAPS_PUREDEVICE)
			behaviorFlags |= D3DCREATE_PUREDEVICE;
	}
	else
		behaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING; //if hardware isn't supported (just stop here and upgrade your video card)

	{
		m_D3dPP.Windowed = true;
		m_D3dPP.SwapEffect = D3DSWAPEFFECT_DISCARD;
		m_D3dPP.hDeviceWindow = m_MainWndInst;
		m_D3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		m_D3dPP.BackBufferCount = 1;
		m_D3dPP.MultiSampleType = D3DMULTISAMPLE_NONE;
		m_D3dPP.MultiSampleQuality = 0;
		m_D3dPP.EnableAutoDepthStencil = true;
		m_D3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
		m_D3dPP.Flags = D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
		m_D3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		m_D3dPP.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		m_D3dPP.BackBufferHeight = rect.bottom;
		m_D3dPP.BackBufferWidth = rect.right;
	}//Presentation Parameters

	Test(m_d3Object->CreateDevice(D3DADAPTER_DEFAULT, m_DevType, m_MainWndInst, behaviorFlags, &m_D3dPP, &m_Device));

	m_Device->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &BackBuffer);

	this->initializeDeviceObjects();

}

void GraphicsCore::initializeDeviceObjects()
{

	Test(m_Device->CreateTexture(m_Resolution[0], m_Resolution[1], 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RT[0], NULL));
	Test(m_Device->CreateTexture(m_Resolution[0], m_Resolution[1], 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RT[1], NULL));
	Test(m_Device->CreateTexture(m_Resolution[0], m_Resolution[1], 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RT[2], NULL));
	Test(m_Device->CreateTexture(m_Resolution[0], m_Resolution[1], 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &RT[3], NULL));

	Test(RT[0]->GetSurfaceLevel(0, &RS[0]));
	Test(RT[1]->GetSurfaceLevel(0, &RS[1]));
	Test(RT[2]->GetSurfaceLevel(0, &RS[2]));
	Test(RT[3]->GetSurfaceLevel(0, &RS[3]));

	ID3DXBuffer* errors = 0;
	Test(D3DXCreateEffectFromFileA(m_Device, "../shaders/PhongShader.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &Build, &errors));
	if (errors)
	{
		string wrong = (char*)errors->GetBufferPointer();
	}
	D3DXCreateEffectFromFileA(m_Device, "../shaders/DeferredLightPass.fx", 0, 0, D3DXFX_NOT_CLONEABLE | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_DEBUG, 0, &Light, &errors);
	if (errors)
	{
		string wrong = (char*)errors->GetBufferPointer();
		int hi = 0;
	}
	Build->SetTechnique("main");
	//Light->SetTechnique("main");

}

void GraphicsCore::beginRender()
{
	Test(m_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.0f), 1.0f, 0));
	Test(m_Device->BeginScene());

	/*m_Device->ColorFill(RS[0], NULL, D3DXCOLOR(1, 0, 0, 1));
	m_Device->ColorFill(RS[1], NULL, D3DXCOLOR(1, 1, 0, 1));
	m_Device->ColorFill(RS[2], NULL, D3DXCOLOR(1, 1, 1, 1));
	m_Device->ColorFill(RS[3], NULL, D3DXCOLOR(0, 1, 1, 1));

	m_Device->SetRenderTarget(0, RS[0]);
	m_Device->SetRenderTarget(1, RS[1]);
	m_Device->SetRenderTarget(2, RS[2]);
	m_Device->SetRenderTarget(3, RS[3]);*/

}

void GraphicsCore::endRender()
{

	Test(m_Device->EndScene());
	Test(m_Device->Present(0, 0, 0, 0));

}

void GraphicsCore::renderMesh(UINT mesh, D3DXMATRIX trans)
{

	static UINT numPasses = 0;
	D3DXMATRIX vp = KRNL->GetCameraPtr()->getViewProjMatrix();
	Build->SetMatrix("WVP", &(trans * KRNL->GetCameraPtr()->getViewProjMatrix()));
	Build->SetMatrix("World", &trans);
	D3DXMATRIX invtrans;
	D3DXMatrixInverse(&invtrans, 0, &trans);
	Build->SetMatrix("WorldInvTrans", &invtrans);
	Build->SetValue("camPos", &KRNL->GetCameraPtr()->getPos(), sizeof(D3DXVECTOR3));

	Test(Build->Begin(&numPasses, D3DXFX_DONOTSAVESTATE));

	for (int j = 0; j < KRNL->GetRMPtr()->getMesh(mesh)->getNumMaterials(); j++)
	{

		Build->SetTexture("Tex", KRNL->GetRMPtr()->getMesh(mesh)->getTexture(j));
		Test(Build->BeginPass(0));
		Test(KRNL->GetRMPtr()->getMesh(mesh)->getMesh()->DrawSubset(j));
		Test(Build->EndPass());

	}
	Test(Build->End());

}

void GraphicsCore::Test(HRESULT in)
{
	if (in == S_OK || in == D3D_OK)
		string greatsuccess = "winning";
	else
	{
		MessageBoxA(NULL, "It is error!", "weewoo", NULL);
	}
}

int GraphicsCore::ScreenCenter(bool x)
{
	if (x)
		return m_WindowPosition[0] + (m_Resolution[0] / 2);
	else
		return m_WindowPosition[1] + (m_Resolution[1] / 2);
}
