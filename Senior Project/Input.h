#pragma once

#include "../inc/d3dx9.h"
#include <map>
#include <Windows.h>

using namespace std;

struct CMD
{
	char cmd;
	void* extra;
};

class Input
{
	BYTE* buffer;
	RAWINPUTDEVICE devs[2];

	RAWINPUT lastRaw;
	std::map <char, char > keybinds;
	POINT* mousePos;

	POINT* lastPos;

	float dx, dy;
public:
	Input();
	~Input(void);

	CMD* handleMessage(UINT msg, WPARAM wParam, LPARAM lParam);
	bool keyPressed(int button);
	bool mousePressed(int button);
	POINT getMousePos();
	void setMousePos(POINT *point);
	void setDX(float dx);
	void setDY(float dy);
	void setRegMouse(bool in);
	void saveKeybinds();
	bool getregMouse();
	float getLX();
	float getLY();
};