#pragma once
//#include "DXUtil.h"
#include "../inc/d3dx9.h"
#include <d3d9.h>
#include <string>

using namespace std;

class Mesh
{
	LPD3DXMESH mesh;
	DWORD numMaterials;
	LPD3DXBUFFER buffer;
	LPDIRECT3DTEXTURE9* texture;
	D3DMATERIAL9* material;
	D3DXMATERIAL* inMat;
	UINT tech;
	string path;
	D3DXVECTOR3 bounds;
public:
	Mesh(string path);
	~Mesh(void);
	int getNumMaterials();
	LPDIRECT3DTEXTURE9 getTexture(int i);
	LPDIRECT3DTEXTURE9 getTextures();
	D3DMATERIAL9* getMaterial();
	LPD3DXMESH getMesh();
	D3DXVECTOR3 getBounds();
	UINT getTech();
	UINT getNumVerts();
	string getPath();
};

