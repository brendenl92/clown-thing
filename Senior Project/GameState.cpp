#include "GameState.h"
#include "input.h"
#include "Kernel.h"
#include "Entity.h"


GameState::GameState()
: State()
{
	fwd = bak = lft = rgt = up = dwn = mvup = mvdn = mvrt = mvlt = false;

	 StartPosition = D3DXVECTOR3(20, 0, 200);
}


GameState::~GameState()
{
}
void GameState::Initialize()
{
	KRNL->showCursor(false);
	Entity* entity = new Entity();
	TreeDeeComponent* dd = new TreeDeeComponent();
	dd->initialize(&string("../models/crate.x"));
	dd->setScale(D3DXVECTOR3(15, 3, 3.5));
	entity->addComponent(dd);
	entity->SetPosition(D3DXVECTOR3(0, 0, -8));
	Entity* entity2 = new Entity();
	TreeDeeComponent* d = new TreeDeeComponent();
	d->initialize(&string("../models/crate.x"));
	d->setScale(D3DXVECTOR3(15, 3, 3.5));
	entity2->addComponent(d);
	entity2->SetPosition(D3DXVECTOR3(0, 0, 8));
	Entity* entity3 = new Entity();
	TreeDeeComponent* ddd = new TreeDeeComponent();
	ddd->initialize(&string("../models/crate.x"));
	ddd->setScale(D3DXVECTOR3(3.5, 3, 15));
	entity3->addComponent(ddd);
	entity3->SetPosition(D3DXVECTOR3(-8, 0, 0));
	Entity* entity4 = new Entity();
	TreeDeeComponent* dddd = new TreeDeeComponent();
	dddd->initialize(&string("../models/crate.x"));
	dddd->setScale(D3DXVECTOR3(3.5, 3, 15));
	entity4->addComponent(dddd);
	entity4->SetPosition(D3DXVECTOR3(8, 0, 0));
	Entity* entity5 = new Entity();
	TreeDeeComponent* ddddd = new TreeDeeComponent();
	ddddd->initialize(&string("../models/crate.x"));
	ddddd->setScale(D3DXVECTOR3(8, 3, 10));
	entity5->SetPosition(D3DXVECTOR3(0, 0, 2));
	entity5->addComponent(ddddd);
	Entity* entity7 = new Entity();
	TreeDeeComponent* dddddd = new TreeDeeComponent();
	dddddd->initialize(&string("../models/base.x"));
	dddddd->setScale(D3DXVECTOR3(20, .001f, 20));
	entity7->SetPosition(D3DXVECTOR3(0, 0, 0));
	entity7->addComponent(dddddd);

	///Codi Made...
	KRNL->CreateEntity(entity);
	KRNL->CreateEntity(entity2);
	KRNL->CreateEntity(entity3);
	KRNL->CreateEntity(entity4);
	KRNL->CreateEntity(entity5);
	KRNL->CreateEntity(entity7);
	
	//Codi Added this

}


void GameState::UpdateScene(float dt)
{


	if (rgt)
		KRNL->GetCameraPtr()->adjustBOR(.05f);
	if (lft)
		KRNL->GetCameraPtr()->adjustBOR(-.05f);
	if (fwd)
		KRNL->GetCameraPtr()->adjustBOF(.05f);
	if (bak)
		KRNL->GetCameraPtr()->adjustBOF(-.05f);

	if (up)
		KRNL->GetCameraPtr()->adjustBOU(.05f);
	if (dwn)
		KRNL->GetCameraPtr()->adjustBOU(-.05f);

}
void GameState::HandleInput(CMD* cmd)
{

	switch(cmd->cmd)
	{
	case 126:
	{
				D3DXVECTOR4* derp = (D3DXVECTOR4*)cmd->extra;
				KRNL->GetCameraPtr()->update(derp->x, derp->y);
				delete cmd->extra;
				break;
	}
	case 124:
	{
				//int pause = 0;
				exit(10);
				break;
	}
		case 1:
			if ((char*)cmd->extra == (char*)1)
				fwd = true;
			else
				fwd = false;
			break;
		case 2:
			if ((char*)cmd->extra == (char*)1)
				lft = true;
			else
				lft = false;
 			break;
		case 3:
			if ((char*)cmd->extra == (char*)1)
				bak = true;
			else
				bak = false;
			break;
		case 4:
			if ((char*)cmd->extra == (char*)1)
				rgt = true;
			else
				rgt = false;
			break;
		case 5:
			if ((char*)cmd->extra == (char*)1)
				up = true;
			else
				up = false;	
			break;
		case 6:
			if ((char*)cmd->extra == (char*)1)
				dwn = true;
			else
				dwn = false;
			break;
		case 7:
			if ((char*)cmd->extra == (char*)1)
				mvup = true;
			else
				mvup = false;
			break;
		case 8:
			if ((char*)cmd->extra == (char*)1)
				mvlt = true;
			else
				mvlt = false;
			break;
		case 9:
			if ((char*)cmd->extra == (char*)1)
				mvdn = true;
			else
				mvdn = false;
			break;
		case 10:
			if ((char*)cmd->extra == (char*)1)
				mvrt = true;
			else
				mvrt = false;
			break;
	};

	
}
void GameState::Leave()
{

}

