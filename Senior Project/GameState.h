#pragma once

#include "State.h"

#include <d3d9.h>
#include "../inc/d3dx9.h"

class Entity;
class GameState : public State
{
private:
	bool fwd, bak, lft, rgt, up, dwn, mvup, mvdn, mvrt, mvlt;

	D3DXVECTOR3 StartPosition;
	Entity* player;

public:
	GameState();
	~GameState();

	void Initialize();
	void UpdateScene(float dt);
	void HandleInput(CMD* cmd);
	void Leave();

	void PenalizePlayers();
};

