#pragma once
#include "FSM.h"
#include "Audio.h"
#include "Entity.h"
#include "GraphicsCore.h"
#include "Settings.h"
#include "Input.h"
#include "Camera.h"
#include "ResourceManager.h"
#include "Clock.h"
#include <time.h>
#include <thread>
#include <DbgHelp.h>
#include <vector>

class Kernel
{

	bool paused;
	
	Clock* clock;
	Camera* camera;
	FSM* m_FSM;
	Audio* Sound; 
	GraphicsCore* Graphics;
	ResourceManager* RM;
	Input* input;
	Settings* settings;
	HINSTANCE m_Instance;

	int m_Resolution[2];

	vector<Entity*> m_Entities;
	

public:
	
	vector<Entity*> m_Players;
	vector<Entity*> m_Paths;

	thread AudioT;
	thread InputT;

	Kernel();
	~Kernel();
	void Startup(HINSTANCE instance);
	void update();

	HINSTANCE getInstance();

	LRESULT handleWindowsMessages(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	void forwardMail(Mail* mail);

	void CreateEntity(char type, char AI_type, FLOAT mass, FLOAT damp, int shape, D3DXVECTOR3 halfsize, D3DXVECTOR3 pos, D3DXVECTOR3 vel);
	void CreateEntity(Entity* entity);

	void CreatePlayer(Entity* entity);
	void CreatePath(Entity* entity);

	vector<Entity*> GetEntities() const;
    
	inline GraphicsCore* GetGraphicsPtr(){ return Graphics; }
	inline Camera* GetCameraPtr(){ return camera; }
	inline ResourceManager* GetRMPtr(){ return RM; }
	inline Clock* getClockPtr(){ return clock; }

	void fatalError(string reason, void* extradata);
	void showCursor(bool);

	inline void ChangeState(State* state){ m_FSM->changeState(state); }
	Clock* GetClock(){ return clock; }

};

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

extern Kernel* KRNL;