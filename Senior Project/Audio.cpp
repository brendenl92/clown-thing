#include "Audio.h"
#include "Clock.h"
#include "Kernel.h"

Audio::Audio()
{
	m_MailBox = new MailBox();
}


Audio::~Audio()
{
}

void ERRCHECK(FMOD_RESULT result, Audio* play)
{
	if (result != FMOD_OK)
	{
		//std::cout << "FMOD Error: " << FMOD_ErrorString(result);
		play->playSound("../media/sounds/nein.mp3");
		exit(-1);
	}
} 

void Audio::startup()
{
	FMOD_RESULT      result;
	unsigned int     version;
	int              numdrivers;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS        caps;
	char             name[256];


	result = FMOD::System_Create(&system);
	ERRCHECK(result, this);

	result = system->getVersion(&version);
	ERRCHECK(result, this);

	if (version < FMOD_VERSION)
	{
		//cout << "Error!  You are using an old version of FMOD " << version << ".  This program requires " << FMOD_VERSION << ".";
		return;
	}

	result = system->getNumDrivers(&numdrivers);
	ERRCHECK(result, this);

	if (numdrivers == 0)
	{
		result = system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		ERRCHECK(result, this);
	}
	else
	{
		result = system->getDriverCaps(0, &caps, 0, &speakermode);
		ERRCHECK(result, this);

		result = system->setSpeakerMode(FMOD_SPEAKERMODE_7POINT1);
		ERRCHECK(result, this);
		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			result = system->setDSPBufferSize(1024, 10);
			ERRCHECK(result, this);
		}
		result = system->getDriverInfo(0, name, 256, 0);
		ERRCHECK(result, this);
		if (strstr(name, "SigmaTel"))
		{
			result = system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR);
			ERRCHECK(result, this);
		}
	}
	result = system->init(100, FMOD_INIT_NORMAL, 0);
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		result = system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		ERRCHECK(result, this);

		result = system->init(100, FMOD_INIT_NORMAL, 0);
	}
	ERRCHECK(result, this);

	result = system->createReverb(&reverb);
	ERRCHECK(result, this);

	this->update();

}

void Audio::update()
{
	Mail* currMsg;
	bool shutdown = false;

	do
	{
		currMsg = m_MailBox->getNext();
		if (currMsg != nullptr)
		{
			if (currMsg->message == PLAYSOUND)
				this->playSound(((string*)(currMsg->data))->c_str());
			if (currMsg->message == SHUTDOWN)
				shutdown = true;
			delete currMsg;
		}

	} while (shutdown == false);

}

void Audio::playSound(string name)
{

		FMOD_RESULT	result;
		FMOD::Channel *channel = 0;
		FMOD::Sound *sound = 0;

		result = system->createSound(name.c_str(), FMOD_DEFAULT, 0, &sound);
		ERRCHECK(result, this);
		unsigned int length;
		sound->getLength(&length, FMOD_TIMEUNIT_MS);

		effects.push_back(make_pair(sound, KRNL->getClockPtr()->getDT() + length));
		effects.push_back(make_pair(sound, KRNL->GetClock()->getTimeElapsed() + length));

		result = system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel);
		ERRCHECK(result, this);

		result = channel->setVolume(1);
		ERRCHECK(result, this);

		result = channel->setPaused(false);
		ERRCHECK(result, this);

}

void Audio::addMessage(Mail* mail)
{
	m_MailBox->setMail(mail);
}