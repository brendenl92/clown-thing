#include "FSM.h"
#include "Input.h"

FSM::FSM()
{
}


FSM::~FSM()
{
}

void FSM::setCurrentState(State* state)
{
	m_LastState = m_CurrentState;
	m_CurrentState = state;
	state->Initialize();
	m_CurrentState = state;
}

void FSM::changeState(State* state)
{
	m_CurrentState->Leave();
	m_LastState = m_CurrentState;
	m_CurrentState = state;
	m_CurrentState->Initialize();
}

void FSM::handleInput(CMD* cmd)
{
	int hi = 0;
	m_CurrentState->HandleInput(cmd);
}
void FSM::update(float dt)
{
	m_CurrentState->UpdateScene(dt);
}