#include "Entity.h"

Entity::Entity(void)
{
	mRigidEnabled = false;
	D3DXQuaternionIdentity(&orientation);
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	velocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}


Entity::~Entity(void)
{

}

void Entity::addComponent(BaseComponent* addition)
{
	components.push_back(addition);
}

void Entity::CreateComponent(char type, char AI_type, FLOAT m, FLOAT damp, int shape, D3DXVECTOR3 h_size, D3DXVECTOR3 pos, D3DXVECTOR3 vel)
{
	position = pos;
	velocity = vel;
}

void Entity::Update(float dt)
{

	for (int i = 0; i < components.size(); i++)
		components[i]->update(this);
}

