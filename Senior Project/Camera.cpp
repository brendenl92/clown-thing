#include "Camera.h"


Camera::Camera()
{

	m_Position = D3DXVECTOR3(0, 29, 0);
	m_Forward = D3DXVECTOR3(0.0f, -1.0f, 0.0f);
	m_Right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	m_Up = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

	m_Sensitivity = 50;

	D3DXMatrixIdentity(&m_ViewMatrix);
	D3DXMatrixIdentity(&m_ProjMatrix);

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix, D3DXToRadian(30.0f), 1.0f, 1.0f, 1024.0f);

	BuildViewProjMatrix();

}


Camera::~Camera()
{
}

void Camera::recalcVectors()
{

	m_Right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);	
	m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);	

	m_Forward = m_Forward - m_Position;
	D3DXVec3Normalize(&m_Forward, &m_Forward);

	D3DXVec3Cross(&m_Right, &m_Up, &m_Forward);
	D3DXVec3Cross(&m_Up, &m_Forward, &m_Right);

}

void Camera::BuildViewProjMatrix()
{
	D3DXVec3Normalize(&m_Forward, &m_Forward);

	D3DXVec3Cross(&m_Up, &m_Forward, &m_Right);
	D3DXVec3Normalize(&m_Up, &m_Up);

	D3DXVec3Normalize(&m_Right, &m_Right);
	float x, y, z;

	//Fill in the view matrix entries.
	x = -D3DXVec3Dot(&m_Position, &m_Right);
	y = -D3DXVec3Dot(&m_Position, &m_Up);
	z = -D3DXVec3Dot(&m_Position, &m_Forward);

	m_ViewMatrix(0, 0) = m_Right.x;
	m_ViewMatrix(1, 0) = m_Right.y;
	m_ViewMatrix(2, 0) = m_Right.z;
	m_ViewMatrix(3, 0) = x;

	m_ViewMatrix(0, 1) = m_Up.x;
	m_ViewMatrix(1, 1) = m_Up.y;
	m_ViewMatrix(2, 1) = m_Up.z;
	m_ViewMatrix(3, 1) = y;

	m_ViewMatrix(0, 2) = m_Forward.x;
	m_ViewMatrix(1, 2) = m_Forward.y;
	m_ViewMatrix(2, 2) = m_Forward.z;
	m_ViewMatrix(3, 2) = z;

	m_ViewMatrix(0, 3) = 0.0f;
	m_ViewMatrix(1, 3) = 0.0f;
	m_ViewMatrix(2, 3) = 0.0f;
	m_ViewMatrix(3, 3) = 1.0f;

	m_ViewProjMatrix = m_ViewMatrix * m_ProjMatrix;
}

void Camera::adjustPos(D3DXVECTOR3 in)
{
	m_Position += in;
	this->BuildViewProjMatrix();

}
void Camera::adjustBOF(float in)
{
	m_Position += (m_Forward * in);
	this->BuildViewProjMatrix();

}
void Camera::adjustBOR(float in)
{
	m_Position += m_Right * in;
	this->BuildViewProjMatrix();

}
void Camera::adjustBOU(float in)
{
	m_Position += m_Up * in;
	this->BuildViewProjMatrix();

}

void Camera::update(float ix, float iy)
{

	float x = ix * .000033f;
	float y = iy * .000033f;

	x *= m_Sensitivity;
	y *= m_Sensitivity;

	if (y != 0.0f)
	{
		//rotate Up and Down
		D3DXMATRIX xRot;
		D3DXMatrixRotationAxis(&xRot, &m_Right, y);
		D3DXVec3TransformCoord(&m_Forward, &m_Forward, &xRot);
		D3DXVec3TransformCoord(&m_Up, &m_Up, &xRot);
	}
	if (x != 0.0f)
	{
		//rotate left and right
		D3DXMATRIX yRot;
		D3DXMatrixRotationY(&yRot, x);
		D3DXVec3TransformCoord(&m_Right, &m_Right, &yRot);
		D3DXVec3TransformCoord(&m_Up, &m_Up, &yRot);
		D3DXVec3TransformCoord(&m_Forward, &m_Forward, &yRot);
	}

	this->BuildViewProjMatrix();



}

void Camera::reset(D3DXVECTOR3 pos)
{
	m_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_Forward = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	m_Right = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	m_Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}

void Camera::setPos(D3DXVECTOR3 position)
{
	m_Position = position;
}

void Camera::setTarget(D3DXVECTOR3 target)
{
	m_Forward = target;
}

void Camera::setSensitivity(float sen)
{
	m_Sensitivity = sen;
}

D3DXVECTOR3 Camera::getPos()
{
	return m_Position;
}

D3DXVECTOR3 Camera::getForward()
{
	return m_Forward;
}

D3DXVECTOR3 Camera::getRight()
{
	return m_Right;
}

D3DXVECTOR3 Camera::getUp()
{
	return m_Up;
}

D3DXMATRIX Camera::getViewProjMatrix()
{
	return m_ViewProjMatrix;
}
