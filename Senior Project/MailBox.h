#pragma once
#include "Mail.h"

enum CORES
{
	//INPUT,
	SOUND,
	PHYSICS,
	AI,
	GRAPHICS
};

class MailBox
{
	std::vector<Mail*> MailBin;
public:
	MailBox();
	~MailBox();
	Mail* getNext();
	void setMail(Mail* in);
};

