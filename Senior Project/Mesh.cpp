#include "Mesh.h"
#include "Kernel.h"

Mesh::Mesh(string path)
{
	this->path = path;

	HRESULT yes = D3DXLoadMeshFromXA(path.c_str(), D3DXMESH_MANAGED, KRNL->GetGraphicsPtr()->GetDevice(), NULL, &buffer, NULL, &numMaterials, &mesh);

	inMat = (D3DXMATERIAL*)buffer->GetBufferPointer();

	material = new D3DMATERIAL9[numMaterials];
	texture = new LPDIRECT3DTEXTURE9[numMaterials];

	for (UINT i = 0; i < numMaterials; i++)
	{
		material[i] = inMat[i].MatD3D;
		material[i].Ambient = material[i].Diffuse;
		char file[100] = "../media/textures/";
		if (inMat[i].pTextureFilename)
		{
			strcat_s(file, inMat[i].pTextureFilename);
		}
		if (FAILED(D3DXCreateTextureFromFileA(KRNL->GetGraphicsPtr()->GetDevice(), file, &texture[i])))
			D3DXCreateTextureFromFileA(KRNL->GetGraphicsPtr()->GetDevice(), "../media/textures/nopenopenope.jpg", &texture[i]);
	}

}

UINT Mesh::getNumVerts()
{
	return (UINT)mesh->GetNumVertices();
}

Mesh::~Mesh()
{
	for (UINT i = 0; i < numMaterials; i++)
		if (texture[i])
			texture[i]->Release();
	delete texture;
	mesh->Release();
	if (material)
		delete material;
	buffer->Release();
}

string Mesh::getPath()
{
	return path;
}

LPDIRECT3DTEXTURE9 Mesh::getTextures()
{

	return texture[0];

}


D3DMATERIAL9* Mesh::getMaterial()
{

	return material;

}

LPDIRECT3DTEXTURE9 Mesh::getTexture(int i)
{

	return texture[i];

}

int Mesh::getNumMaterials()
{

	return (int)numMaterials;

}

LPD3DXMESH Mesh::getMesh()
{

	return mesh;

}

UINT Mesh::getTech()
{
	return tech;
}