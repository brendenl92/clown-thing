#include "Clock.h"

Clock* CLK = nullptr;

Clock::Clock()
{

}


Clock::~Clock()
{

}

void Clock::initialize()
{
	start = std::chrono::system_clock::now();
	frame = std::chrono::system_clock::now();
	dt = .003f;
}

float Clock::getTimeElapsed()
{
	return elapsedTime;
}

float Clock::getDT()
{
	return dt;
}

void Clock::beginFrame()
{
	frame = std::chrono::system_clock::now();
}

void Clock::endFrame()
{
	std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
	frame = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = now - frame;
	dt = (float)elapsed_seconds.count();
	elapsedTime += dt;
}