#pragma once

#include "../inc/d3d9.h"
#include "../inc/d3dx9.h"
#include "../inc/DxErr.h"
#include "Mesh.h"
#include <string>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


using namespace std;

struct planeVertex {
	float x, y, z, w;
	float u, v;
};

class GraphicsCore
{

	D3DCAPS9 caps;
	D3DADAPTER_IDENTIFIER9 ID;
	IDirect3DDevice9* m_Device;
	D3DDEVTYPE m_DevType;
	DWORD m_RequestedVP;
	HINSTANCE m_AppInst;
	HWND m_MainWndInst;
	IDirect3D9* m_d3Object;
	D3DPRESENT_PARAMETERS m_D3dPP;

	int m_Resolution[2];
	int m_WindowPosition[2];

	IDirect3DTexture9* RT[4];
	IDirect3DSurface9* RS[4];
	IDirect3DSurface9* BackBuffer;

	LPD3DXEFFECT Build;
	LPD3DXEFFECT Light;

	planeVertex planeVerts[4];

	void initializeDeviceObjects();

public:
	GraphicsCore();
	~GraphicsCore();

	void initialize();



	void beginRender();
	void endRender();

	void renderMesh(UINT mesh, D3DXMATRIX trans);

	inline IDirect3DDevice9* GetDevice()
	{
		return m_Device;
	}
	HWND getHWND()
	{
		return m_MainWndInst;
	}
	void Test(HRESULT in);

	int ScreenCenter(bool x);

	inline int GetWindowWidth(){ return m_Resolution[0]; }
	inline int GetWindowHeight(){ return m_Resolution[1]; }
};