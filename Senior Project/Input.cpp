#include "Input.h"
#include <iostream>
#include "Kernel.h"

Input::Input()
{

	devs[0].usUsagePage = 1;
	devs[0].usUsage = 6;
	devs[0].dwFlags = 0;
	devs[0].hwndTarget = NULL;

	devs[1].usUsagePage = 1;
	devs[1].usUsage = 2;
	devs[1].dwFlags = 0;
	devs[1].hwndTarget = NULL;

	mousePos = new POINT;
	GetCursorPos(mousePos);

	if (!RegisterRawInputDevices(devs, 2, sizeof(RAWINPUTDEVICE)))
		KRNL->fatalError("RAW FAILED", NULL);

	buffer = new BYTE[40];

	dx = 0;
	dx = 0;

}

Input::~Input(void)
{
	if (buffer)
		delete buffer;
	if (mousePos)
		delete mousePos;
	if (lastPos)
		delete lastPos;

}

void Input::setMousePos(POINT *point)
{
	this->mousePos = point;
}
void Input::setDX(float i)
{
	dx = i;
}
void Input::setDY(float i)
{
	dy = i;
}

POINT Input::getMousePos()
{
	return *mousePos;
}

float Input::getLX()
{
	float temp = dx;
	dx = 0;
	return temp;
}

float Input::getLY()
{
	float temp = dy;
	dy = 0;
	return temp;
}

CMD* Input::handleMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{

	UINT bufferSize;
	GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &bufferSize, sizeof (RAWINPUTHEADER));
	if (bufferSize <= 40)
		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, (LPVOID)buffer, &bufferSize, sizeof (RAWINPUTHEADER));

	RAWINPUT raw = *(RAWINPUT*)buffer;

	CMD* command = new CMD();

	if (raw.header.dwType == RIM_TYPEMOUSE)
	{


		if (raw.data.mouse.usButtonFlags != lastRaw.data.mouse.usButtonFlags)
		{
			if (raw.data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN && lastRaw.data.mouse.usButtonFlags | RI_MOUSE_LEFT_BUTTON_DOWN)
			{
				command->cmd = 125;
				command->extra = (char*)1;
				lastRaw = raw;
				return command;
			}
			else if (raw.data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP && lastRaw.data.mouse.usButtonFlags | RI_MOUSE_LEFT_BUTTON_UP)
			{
				command->cmd = 125;
				command->extra = (char*)0;
				lastRaw = raw;
				return command;
			}
		}
		else
		{
			command->cmd = 126;
			GetCursorPos(mousePos);
			command->extra = new D3DXVECTOR4((float)raw.data.mouse.lLastX, (float)raw.data.mouse.lLastY, (float)mousePos->x, (float)mousePos->y);
			lastRaw = raw;
			return command;
		}

	}

	if (raw.header.dwType == RIM_TYPEKEYBOARD)
	{
		switch (raw.data.keyboard.VKey)
		{
		case VK_ESCAPE:
			command->cmd = 124;
			lastRaw = raw;
			return command;
			break;
		case 0x57:
			command->cmd = 1;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			lastRaw = raw;
			return command;
			break;
		case 0x41:
			command->cmd = 2;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			lastRaw = raw;
			return command;
			break;
		case 0x53:
			command->cmd = 3;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x44:
			command->cmd = 4;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case VK_SPACE:
			command->cmd = 5;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x5A:
			command->cmd = 6;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x49:
			command->cmd = 7;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x4A:
			command->cmd = 8;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x4B:
			command->cmd = 9;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		case 0x4C:
			command->cmd = 10;
			if (raw.data.keyboard.Flags == 0)
				command->extra = (char*)1;
			else
				command->extra = (char*)0;
			return command;
			break;
		}


	}
	//g_D3DAPP->HandleInput(&raw);
	CMD* cmd = new CMD();
	cmd->cmd = 'q';
	cmd->extra = nullptr;
	return cmd;
}

void Input::saveKeybinds()
{

}
