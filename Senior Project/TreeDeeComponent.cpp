#include "TreeDeeComponent.h"
#include "Kernel.h"

TreeDeeComponent::TreeDeeComponent()
{
	Scale = D3DXVECTOR3(1, 1, 1);
}


TreeDeeComponent::~TreeDeeComponent()
{
}

void TreeDeeComponent::initialize(void* info)
{

	string* name = (string*)info;
	UINT id = KRNL->GetRMPtr()->doesMeshExist(name);
	if (id != -1)
		this->mesh = id;
	else
	{
		UINT hi  = KRNL->GetRMPtr()->ImportMeshes(name, 1);
		this->mesh = hi;
	}

}

void TreeDeeComponent::update(Entity* entity)
{

	D3DXMATRIX transform;
	D3DXMatrixTransformation(&transform, 0, 0, &Scale, 0, &entity->GetRotation(), &entity->GetPosition());
	KRNL->GetGraphicsPtr()->renderMesh(this->mesh, transform);

}

void TreeDeeComponent::shutdown()
{

}

void TreeDeeComponent::setScale(D3DXVECTOR3 scale)
{
	Scale = scale;
}