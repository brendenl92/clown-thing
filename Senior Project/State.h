#pragma once

#include <windows.h>

using namespace std;

struct CMD;

class State
{
public:
	State(void){};
	~State(void){};

	virtual void Initialize() = 0;
	virtual void UpdateScene(float dt) = 0;
	virtual void HandleInput(CMD* input) = 0;
	virtual void Leave() = 0;

};

